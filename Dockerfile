FROM gitlab/gitlab-ce:8.7.1-ce.1
MAINTAINER Joseph Weston

# fail2ban to quiet the bruteforce attacks on SSH
RUN apt-get update &&\
    apt-get install -y fail2ban

# copy over assets for rebranding
COPY images/* /opt/gitlab/embedded/service/gitlab-rails/app/assets/images/
COPY templates/appearances_helper.rb /opt/gitlab/embedded/service/gitlab-rails/app/helpers/
COPY templates/devise.html.haml /opt/gitlab/embedded/service/gitlab-rails/app/views/layouts/
# patches to wrapper script and Gitlab rake -- related to rebuilding assets
COPY patches/* /assets/
RUN patch /assets/wrapper /assets/wrapper.patch
